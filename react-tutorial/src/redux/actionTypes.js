import {ADD_TODO, TOGGLE_TODO} from "./actionTypes";

let nextTodoId = 0;

export const addTodo = content =>{
    return {
        type, ADD_TODO,
        playload: {
            id: ++nextTodoId,
            content,
        }
    }
};

export const toggleTodo = id => ({
    type: TOGGLE_TODO,
    playload: {
        id
    }
});